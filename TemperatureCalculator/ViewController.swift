//
//  ViewController.swift
//  TemperatureCalculator
//
//  Created by IDS Comercial on 05/01/18.
//  Copyright © 2018 IDS Comercial. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var enterLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var formatSeg: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func calculate(_ sender: Any) {//Va a mandar a hacer el calculo cuando se de enter, esto es con Return Key Done y al hacerlo aqui Connection Action y Event Did End On Exit, Esto al declararlo como accion.
        
        self.resignFirstResponder()//Para q desaparesca el boton al darle enter
        
        if formatSeg.selectedSegmentIndex == 0{
            
            let  fahrenheit = Double(textField.text!) //Para convertir el texto a double se pone ! en text
            
            let celsius = (fahrenheit! - 32) / 1.8
            
            outputLabel.text = String(format: "%4.2f Celsius", celsius) //Para colocar en la cadena la constante celsius Con formato y q ponga 4 dig antes del . y 2 dig despues del .
            
            if celsius > 120 {
                imageView.image = UIImage(named: "Temp9.png") //Coloca la imagen
            }else if celsius > 100 {//120
                imageView.image = UIImage(named: "Temp8.png") //Coloca la imagen
            }else if celsius > 80 {//100
                imageView.image = UIImage(named: "Temp7.png") //Coloca la imagen
            }else if celsius > 60 {//80
                imageView.image = UIImage(named: "Temp6.png") //Coloca la imagen
            }else if celsius > 40 {//60
                imageView.image = UIImage(named: "Temp5.png") //Coloca la imagen
            }else if celsius > 20 {//40
                imageView.image = UIImage(named: "Temp4.png") //Coloca la imagen
            }else if celsius > 0 {//20
                imageView.image = UIImage(named: "Temp3.png") //Coloca la imagen
            }else if celsius > -20 {//0
                imageView.image = UIImage(named: "Temp2.png") //Coloca la imagen
            }else if celsius < -20 {//-20
                imageView.image = UIImage(named: "Temp1.png") //Coloca la imagen
            }
            
        }//segment if == 0
        
        if formatSeg.selectedSegmentIndex == 1{
            
            let celsius = Double(textField.text!)
            
            let fahrenheit = celsius! * 1.8 + 32
            
            outputLabel.text = String(format: "%4.2f Fahrenheit", fahrenheit)
            
            if fahrenheit > 160 {
                imageView.image = UIImage(named: "Temp9.png") //Coloca la imagen
            }else if fahrenheit > 140 {//160
                imageView.image = UIImage(named: "Temp8.png") //Coloca la imagen
            }else if fahrenheit > 120 {//140
                imageView.image = UIImage(named: "Temp7.png") //Coloca la imagen
            }else if fahrenheit > 100 {//120
                imageView.image = UIImage(named: "Temp6.png") //Coloca la imagen
            }else if fahrenheit > 80 {//100
                imageView.image = UIImage(named: "Temp5.png") //Coloca la imagen
            }else if fahrenheit > 60 {//80
                imageView.image = UIImage(named: "Temp4.png") //Coloca la imagen
            }else if fahrenheit > 40 {//60
                imageView.image = UIImage(named: "Temp3.png") //Coloca la imagen
            }else if fahrenheit > 20 {//40
                imageView.image = UIImage(named: "Temp2.png") //Coloca la imagen
            }else if fahrenheit < 20 {//20
                imageView.image = UIImage(named: "Temp1.png") //Coloca la imagen
            }
        }//segment if == 1
        
    }//Calculate
    
    
    @IBAction func switchFormat(_ sender: Any) {
        
        if formatSeg.selectedSegmentIndex == 0{
            
            enterLabel.text = "Enter Fahrenheit"
            textField.text = ""
            outputLabel.text = "0 Celsius"
            
        }//segment if == 0
        
        if formatSeg.selectedSegmentIndex == 1{
            
            enterLabel.text = "Enter Celsius"
            textField.text = ""
            outputLabel.text = "0 Fahrenheit"
            
        }//segment if == 1
        
    }
    
}//switchFormat

